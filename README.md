!!! Current active development in the sim only branch !!!


# Overview
This is the Master branch of the Baymax repo for the Advance Robotic Systems class. Since it is going to be shared by everyone 
I suggest we establish a workflow. Anyone contributing to this repo will open their own branch and replace this README file with 
a brief description of what they are working on, the changes they make, and the progress they are making. 

Once a certain objective is completed and passes the peer review we will merge the into the master branch. 

I suggest to document you code and progress regularly and not waste commit messeges. 

If there are any question on how to effectively to use the repo, dont hesitate to ask. Its better to double check, rather than 
spending time searching through your commits to find the past version that was good. 

Personally I think Python is the best language to work with in this project especially since we are using Klamp't and that is why I suggest
using the PyCharm IDE. It has a number of features that will be extremely helpful throughout this project. 

Master Branch
-----
January 23rd: 
- Bear essentials that load up the model as well as some basic function that will help with the motion planning. 
- Very basic organizational structure for framework, which will be improved as more code will writen and included in the project

