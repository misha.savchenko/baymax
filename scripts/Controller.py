import time

from utilities.ProjectConstants import ProjectConstants
from robot_api.RobotController import UR5WithGripperController


class Controller:
    def __init__(self, traj):
        ur5 = UR5WithGripperController(ProjectConstants.robot_ip_adress, gripper=False, gravity=[0,0,9.8])

