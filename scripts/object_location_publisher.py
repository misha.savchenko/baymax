#!/usr/bin/env python

import json
import random

import rospy
from baymax.msg import object_pose

from multiprocessing.connection import Listener

with open("src/baymax/scripts/data/workspace/reachability_workspace.json", "r") as rw:
    workspace_points = json.load(rw)
points = workspace_points["workspace_points"]

def child(conn):
    while True:
        mess = conn.recv()
        conn.send(mess)

def mother(address):
    server = Listener(address)
    while True:
        client = server.accept()
        child(client)

def talker():


    pub = rospy.Publisher('object_pose', object_pose, queue_size=10)
    rospy.init_node('object_pose', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    stringlist = ["handle", "cup", "box", "person", "table", "phone", "spoon", "pen", "kepler", "dog", "bird"]
    while not rospy.is_shutdown():
        # mother(('', 5000))

        custom_message = object_pose()
        custom_message.object = random.choice(stringlist)
        custom_message.object = "cup"
        pose = [random.uniform(0,1) for i in range(6)]
        pose[:3] = random.choice(points)
        # pose[:3] = [-.6096, 0, .20765]
        custom_message.pose = pose

        rospy.loginfo(custom_message)
        pub.publish(custom_message)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
