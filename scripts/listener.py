#!/usr/bin/env python

import time
import random

import rospy
from baymax.msg import object_pose
from baymax.msg import ur5config

from Planner import Planner
global planner

def callback(data):
    if data.object == "cup" and not planner.currently_planning or data.pose[:3] !=[9999.0, 9999.0, 9999.0]:
        t = list(data.pose[:3])
        print "t", t
        planner.place_cup(t)
        # traj = planner.generate_trajectory(t)
        # planner.execute_trajectory(traj)
        # # print traj.milestones[-1]
        # # print planner.robot.getConfig()
        # execute_trajectory = raw_input("Execute Path on Robot: ")
        # if execute_trajectory == "y" or execute_trajectory == "yes":
        #     planner.execute_trajectory_on_robotB(traj)
        #     # planner.execute_trajectory(traj)
        # else:
        #     return_home = raw_input("Return Home?: ")
        #     if return_home == "y" or return_home == "yes":
        #         traj = planner.return_home()
        #         if traj is not None:
        #             # planner.execute_trajectory_on_robot(traj)
        #             planner.execute_trajectory(traj)

    else:
        print data.pose
        # return_home = raw_input("Return Home?: ")
        # if return_home == "y" or return_home== "yes":
        #     traj = planner.return_home()
        #     if traj is not None:
        #         # planner.execute_trajectory_on_robot(traj)
        #         planner.execute_trajectory(traj)
        # else:
        #     pass

def callback2(ur5config):
    new_config = list(ur5config.config)
    if planner.executing_trajectory is None and not planner.executing_trajectory and not planner.currently_planning:
    # if not planner.executing_trajectory or not planner.currently_planning:
        current_config = planner.robot.getConfig()
        current_config[:len(new_config)] = new_config
        print "changed_config"
        planner.robot.setConfig(new_config)

def listener():

    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('object_pose', object_pose, callback)
    rospy.Subscriber('ur5_config', ur5config, callback2)
    rospy.spin()


if __name__ == '__main__':
    global planner
    planner = Planner()

    listener()
