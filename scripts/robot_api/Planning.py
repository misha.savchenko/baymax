import sys
import time
import json
import random
import numpy as np
from threading import Thread
from heapq import heappop, heappush

sys.path.append("../../")

from klampt import *
from klampt import vis
from klampt.model import ik
from klampt.plan import cspace
from klampt.model import trajectory
from klampt.plan import robotcspace
from klampt.model import coordinates
from klampt.model.collide import WorldCollider

from utilities.DataCore import DataCore
from utilities.RobotPoser import RobotPoser
from utilities.ProjectConstants import ProjectConstants


class Planning(Thread):

    def __init__(self):
        Thread.__init__(self)

        world_name = "flatworld"
        world_file = "../../data/worlds/" + world_name + ".xml"
        robot_file = "../../data/robots/ur5.rob"

        self.world = self.get_world(world_file, robot_file, visualize_robot=True)
        self.visualize_world(self.world)
        self.robot = self.world.robot(0)

        DC = DataCore()
        DC.set_world(self.world)
        DC.set_robot(self.robot)
        self.RP = RobotPoser(DC)
        time.sleep(1)
        with open("../../data/workspace/reachability_workspace.json", "r") as rw:
            self.workspace_points = json.load(rw)

    def get_random_config(self, init_config):
        space = self.generate_space(self.world, self.robot)
        start_time = time.time()
        heap = []
        random_point = self.get_random_workspace_point()

        for i in range(100):
            rand_point_config = self.get_ee_config(space, random_point)
            cost = np.linalg.norm(np.array(rand_point_config) - np.array(init_config))
            heappush(heap, (cost, rand_point_config))
        print(time.time() - start_time)
        end_config = heappop(heap)[1]

        if end_config:
            generated_traj = self.planner(space, init_config, end_config)
            space.close()
            while True:
                self.evaluate_trajectory(generated_traj, dt=0.002)
                # self.RP.eva(generated_traj)
                decision = raw_input("Execute the Trajectory? ")
                if decision == "y":
                    return generated_traj
                elif decision == "n":
                    exit()

    def save_random_config(self, init_config):
        space = self.generate_space(self.world, self.robot)
        start_time = time.time()
        heap = []
        random_point = self.get_random_workspace_point()

        for i in range(100):
            rand_point_config = self.get_ee_config(space, random_point)
            cost = np.linalg.norm(np.array(rand_point_config) - np.array(init_config))
            heappush(heap, (cost, rand_point_config))
        print(time.time() - start_time)
        end_config = heappop(heap)[1]

        if end_config:
            path = self.planner(space, init_config, end_config)
            space.close()
            generated_traj = trajectory.Trajectory(milestones=path)
            generated_traj.save("./UR5e_Control_API_02092019/robot_api")

    def get_random_workspace_point(self):
        points = self.workspace_points["workspace_points"]
        number_of_points = len(points)
        rand_int = random.randint(0, number_of_points)
        random_point = points[rand_int]
        return random_point

    def planner(self, space, qstart, qgoal):
        settings = {'type': 'sbl',
                    'perturbationRadius': 0.125,
                    'bidirectional': True,
                    'shortcut': True,
                    'restart': True,
                    'restartTermCond': "{foundSolution:1,maxIters:1000}"
                    }

        t0 = time.time()
        print("Creating planner...")
        # Manual construction of planner
        planner = cspace.MotionPlan(space, **settings)
        planner.setEndpoints(qstart, qgoal)
        print("Planner creation time", time.time() - t0)
        t0 = time.time()
        print("Planning...")
        numIters = 0
        for round in range(10):
            planner.planMore(500)
            numIters += 1
            if planner.getPath() is not None:
                break
        print("Planning time,", numIters, "iterations", time.time() - t0)

        path = planner.getPath()
        if path is not None:
            dp = space.discretizePath(path)
            print("Feasible Path is found")
            traj = trajectory.RobotTrajectory(self.robot, range(len(dp)), dp)
            vis.add("trajectory", traj)
            return trajectory.Trajectory(milestones=dp)
        else:
            print("Feasible path is not found")
            return None

    def get_world(self, world_file, robot_file, visualize_robot=True):
        world = WorldModel()
        res = world.readFile(world_file)
        if not res:
            raise RuntimeError("Unable to load terrain model")
        if visualize_robot:
            res = world.readFile(robot_file)
            if not res:
                raise RuntimeError("Unable to load robot model")
            world.robot(0).setConfig(ProjectConstants.STARTING_CONFIG)
        return world

    @staticmethod
    def visualize_world(world):
        vis.add("world", world)
        vis.add("coordinates", coordinates.manager())
        vis.setWindowTitle("Height Map GeneratorSuperclass World")
        vp = vis.getViewport()
        vp.w, vp.h = 600, 600
        vis.setViewport(vp)
        vis.autoFitCamera()
        vis.show()

    def generate_space(self, world, robot):
        constraints = self.RP.get_ik_constraints()
        collider = WorldCollider(world)
        space = robotcspace.ClosedLoopRobotCSpace(robot, constraints, collider)
        # space = robotcspace.RobotCSpace(robot, collider=collider)
        space.setup()
        return space

    def get_ee_config(self, space, end_pos):
        robot = self.robot
        max_iter = 1000

        obj = ik.objective(robot.link(ProjectConstants.END_EFFECTOR_IND), local=[0, 0, 0], world=end_pos)

        def feas_check():
            config = self.robot.getConfig()
            return self.validate_config(space, config)

        res = ik.solve_global(obj, iters=max_iter, startRandom=True, feasibilityCheck=feas_check)

        if res:
            end_config = robot.getConfig()
            return end_config
        else:
            return None

    @staticmethod
    def validate_config(space, config, silent=True):
        # TODO: Are the validation the same as in the robosimia
        # TODO: Is self collision detected
        # TODO: Is world object collision detected
        # TODO: Is feasibility detected
        # TODO: Is the config in the closed loop of the provided space
        try:
            for i in range(len(config)):
                if not silent:
                    print("Config #" + str(i))
                    print("space.inbounds(end_config):", space.inBounds(config[i]))
                    try:
                        print("space.closed_loop(end_config):", space.closedLoop(config[i]))
                    except AttributeError:
                        pass
                    print("space.isFeasible(end_config)", space.isFeasible(config[i]))
                    print("space.selfCollision(end_config):", space.selfCollision(config[i]))

                if space.inBounds(config[i]) and space.isFeasible(config[i]) and not space.selfCollision(
                        config[i]) and space.closedLoop(config[i]):
                    pass
                else:
                    return False
            return True

        except TypeError:
            if not silent:
                print("space.inbounds(end_config):", space.inBounds(config))
                try:
                    print("space.closed_loop(end_config):", space.closedLoop(config))
                except AttributeError:
                    pass
                print("space.isFeasible(end_config)", space.isFeasible(config))
                print("space.selfCollision(end_config):", space.selfCollision(config))
            if config is None:
                return False
            if space.inBounds(config) and space.isFeasible(config) and not space.selfCollision(
                    config):# and space.closedLoop(config):
                return True
            else:
                return False

    def evaluate_trajectory(self, generated_traj, dt=ProjectConstants.CONTROLLER_DT):
        start_time = time.time()
        current_config = generated_traj.milestones[0]
        while current_config != generated_traj.milestones[-1]:
            t = time.time() - start_time
            current_config = generated_traj.eval(t)
            print current_config
            self.robot.setConfig(current_config)
            time.sleep(dt)

