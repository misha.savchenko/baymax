#!/usr/bin/env python

import rospy
import random
from baymax.msg import ur5config

import time
from threading import active_count, Thread, Lock
from utilities.ProjectConstants import ProjectConstants

from klampt.math import vectorops
from klampt.model import trajectory


def talker():
    pub = rospy.Publisher('ur5_config', ur5config, queue_size=10)
    rospy.init_node('ur5_config', anonymous=True)
    rate = rospy.Rate(10)  # 10hz
    while not rospy.is_shutdown():
        custom_message = ur5config()
        custom_message.config = [0.0, 0.0, -1.5707963267948966, 0.0, 0.0, 0.0, 0.0, 0.0]
        # custom_message.config = [0.0, -0.2193178694177352, -1.4029676129342625, 0.965544208709491, 0.43735844096379606, 1.3202447907735477, -3.3637874575993405, 0.0]

        rospy.loginfo(custom_message)
        pub.publish(custom_message)
        rate.sleep()

if __name__ == "__main__":
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
