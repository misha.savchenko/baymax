import math
import time
import numpy as np
from heapq import heappop, heappush, heapify

import klampt
from klampt import *
from klampt import vis
from klampt.model import coordinates
from klampt.model import ik, collide
from klampt.math import vectorops, so3
from klampt.vis import GLRealtimeProgram
from klampt.model.collide import WorldCollider
from klampt.model.trajectory import Trajectory
from klampt.model.trajectory import RobotTrajectory
from klampt.plan import cspace, robotplanning, robotcspace

from utilities.ProjectConstants import ProjectConstants
from robot_api.RobotController import UR5WithGripperController

class Planner:

    def __init__(self):
        world_name = "flatworld"
        world_file = "src/baymax/scripts/data/worlds/" + world_name + ".xml"
        # robot_file = "src/baymax/scripts/data/robots/ur5_with_reflex.rob"
        robot_file = "src/baymax/scripts/data/robots/ur5.rob"
        solo_cup_obj = "src/baymax/scripts/data/worlds/objects/better_cup.off"

        self.world = self.get_world(world_file, robot_file, visualize_robot=True)
        self.robot = self.world.robot(0)
        self.solo_cup = self.world.loadRigidObject(solo_cup_obj)
        self.place_cup([1,1,1])
        self.visualize_world(self.world)

        self.currently_planning = False
        self.executing_trajectory = None

        self.ur5 = self.initialize_robot()
        # print "a"
        # self.current_config = ProjectConstants.STARTING_CONFIG

    def initialize_robot(self):
        # Needs to start ur5.start()
        # and shut_down ur5.stop()
        try:
            ur5 = UR5WithGripperController(ProjectConstants.robot_ip_adress, gripper=False, gravity=[0,0,9.8])
        except:
            print "UR5 connection is not initalized!"
            ur5 = None

        return ur5

    def execute_trajectory_on_robot(self, traj):
        self.ur5.start()
        ProjectConstants.HOME_CONFIG = self.ur5.getConfig()
        milestones = traj.milestones
        for config in milestones:
            ur5_config = self.klampt_to_ur5(config)
            self.ur5.setConfig(ur5_config)
            # print self.ur5.getCurrentTime()
            # print self.ur5.getWrench()
            time.sleep(ProjectConstants.CONTROLLER_DT)
        self.current_config = self.ur5_to_klampt(self.ur5.getConfig())
        self.ur5.stop()


    def execute_trajectory_on_robotB(self, input_trajectory, speed=ProjectConstants.CONTROLLER_DT):
        self.ur5.start()
        ProjectConstants.HOME_CONFIG = self.ur5.getConfig()

        self.executing_trajectory = True
        st = time.time()
        ct = time.time()
        while ct - st < ProjectConstants.EXECUTION_TIME - 2*speed :
            config = input_trajectory.eval(ct-st)
            self.robot.setConfig(config)
            ur5_config = self.klampt_to_ur5(config)
            self.ur5.setConfig(ur5_config)
            time.sleep(speed)
            ct = time.time()

        self.ur5.stop()
        self.executing_trajectory = False


    def return_home(self):

        end_config = ProjectConstants.HOME_CONFIG
        print end_config
        init_config = self.current_config
        space = self.generate_space(self.world, self.robot)
        traj = self.planner(space, init_config, end_config)
        # self.currently_planning = False
        return traj


    def generate_trajectory(self, point):
        self.currently_planning = False
        init_config = self.robot.getConfig()
        space = self.generate_space(self.world, self.robot)
        start_time = time.time()
        heap = []

        #
        final_point = np.array(point[:2])
        norm_fp = np.linalg.norm(final_point)
        final_point2 = np.array(final_point[:2]) * (norm_fp  - .1) / np.linalg.norm(norm_fp)
        final_point2 = final_point2.tolist() + [point[2]]
        final_point = final_point.tolist() + [point[2]]

        world_position = [final_point2, final_point]
        local_position = [[0,0.1,0], [0,.2,0]]
        w = []
        for p in local_position:
            # wp = np.array(link.getWorldPosition(p)) - np.array(link.getWorldPosition(localpos[0]))
            # wp += np.array(goal_position)
            # # wp += -1*np.array([-.3,-.3,0])
            # position.append(wp.tolist())
            w.append(self.robot.link(ProjectConstants.END_EFFECTOR_IND).getWorldPosition(p))

        # a = Trajectory(milestones=w)
        # vis.add("a", a)
        #
        # b = Trajectory(milestones=world_position)
        # vis.add("b",b)


        for i in range(100):
            rand_point_config = self.get_ee_config(space, point)
            if rand_point_config is not None:
                cost = np.linalg.norm(np.array(rand_point_config) - np.array(init_config))
                heappush(heap, (cost, rand_point_config))
        print(time.time() - start_time)
        end_config = heappop(heap)[1]
        # print end_config
        # self.robot.setConfig(end_config)
        # time.sleep(100000)
        if end_config:
            traj = self.planner(space, init_config, end_config)
            # self.currently_planning = False
            return traj
        else:
            print "Failed To find a trajectory"
            # self.currently_planning = False
            return None

    def generate_trajectoryB(self, goal_position):
        self.currently_planning = True

        link = self.robot.link(6)
        robot = self.robot
        qstart = robot.getConfig()
        # localpos = [[0, 0, 0], [0.1, 0, 0], [0, 0.1, 0]]
        # goal_position = [.1, .1, .1]

        final_point = np.array(goal_position[:2])
        norm_fp = np.linalg.norm(final_point)
        final_point2 = np.array(final_point[:2]) * (norm_fp  - .1) / np.linalg.norm(norm_fp)
        final_point2 = final_point2.tolist() + [goal_position[2]]
        final_point = final_point.tolist() + [goal_position[2]]

        world_position = [final_point2, final_point]
        local_position = [[0,0,0], [0,.1,0]]
        w = []
        for p in local_position:
            # wp = np.array(link.getWorldPosition(p)) - np.array(link.getWorldPosition(localpos[0]))
            # wp += np.array(goal_position)
            # # wp += -1*np.array([-.3,-.3,0])
            # position.append(wp.tolist())
            w.append(link.getWorldPosition(p))

        a = Trajectory(milestones=w)
        vis.add("a", a)
        # print position
        # print localpos

        # position = [[0.1, 0.5, 0.5], [0.1, 0.6, 0.5], [0.0, 0.5, 0.5]]
        qgoal = None

        while qgoal is None:
            qgoal = self.solve_ik(link, local_position, world_position)
            # print qgoal

        # print qstart
        # print qgoal
        obj = [ik.fixed_rotation_objective(robot.link(6))]
        # for i in range(10, len(qstart)):
        #     obj.append(ik.fixed_objective(robot.link(i)))

        space = robotplanning.makeSpace(self.world, robot, edgeCheckResolution=0.05,
                                        # equalityConstraints=obj,
                                        equalityTolerance=0.01)


        path1 = self.motion_planner(qstart, qgoal, space)
        hand_dict = self.get_hand_dictionary()
        path2 = self.get_hand_trajectory(hand_dict)

        path_comb = path1 + path2
        traj = RobotTrajectory(robot, np.linspace(0, 5, len(path_comb)).tolist(), path_comb)
        self.currently_planning = False
        return traj

    def generate_trajectoryC(self, goal_position):
        self.currently_planning = True

        link = self.robot.link(7)
        robot = self.robot
        qstart = robot.getConfig()
        # localpos = [[0, 0, 0], [0.1, 0, 0], [0, 0.1, 0]]
        # goal_position = [.1, .1, .1]

        final_point = np.array(goal_position[:2])
        norm_fp = np.linalg.norm(final_point)
        final_point2 = np.array(final_point[:2]) * (norm_fp  - .1) / np.linalg.norm(norm_fp)
        final_point2 = final_point2.tolist() + [goal_position[2]]
        final_point = final_point.tolist() + [goal_position[2]]

        world_position = [final_point2, final_point]
        local_position = [[0,0,0], [0,.1,0]]
        w = []
        for p in local_position:
            # wp = np.array(link.getWorldPosition(p)) - np.array(link.getWorldPosition(localpos[0]))
            # wp += np.array(goal_position)
            # # wp += -1*np.array([-.3,-.3,0])
            # position.append(wp.tolist())
            w.append(link.getWorldPosition(p))

        a = Trajectory(milestones=w)
        vis.add("a", a)

        #
        # qgoal = self.solve_ik(link, local_position, world_position)
        # print qstart
        # print qgoal
        # obj = [ik.fixed_rotation_objective(robot.link(6))]

        space = robotplanning.makeSpace(self.world, robot, edgeCheckResolution=0.05,
                                        # equalityConstraints=obj,
                                        equalityTolerance=0.01)

        qgoal = self.get_ee_config(space, final_point2)
        print qgoal
        path1 = self.motion_planner(qstart, qgoal, space)
        # hand_dict = self.get_hand_dictionary()
        # path2 = self.get_hand_trajectory(hand_dict)

        path_comb = path1
        traj = RobotTrajectory(robot, np.linspace(0, 5, len(path_comb)).tolist(), path_comb)
        self.currently_planning = False
        return traj

    def planner(self, space, qstart, qgoal):
        settings = {'type': 'sbl',
                    'perturbationRadius': 0.125,
                    'bidirectional': True,
                    'shortcut': True,
                    'restart': True,
                    'restartTermCond': "{foundSolution:1,maxIters:1000}"
                    }

        t0 = time.time()
        print("Creating planner...")
        # Manual construction of planner
        planner = cspace.MotionPlan(space, **settings)
        planner.setEndpoints(qstart, qgoal)
        print("Planner creation time", time.time() - t0)
        t0 = time.time()
        print("Planning...")
        numIters = 0
        for round in range(10):
            planner.planMore(500)
            numIters += 1
            if planner.getPath() is not None:
                break
        print("Planning time,", numIters, "iterations", time.time() - t0)

        path = planner.getPath()
        if path is not None:
            # traj = RobotTrajectory(self.robot, np.linspace(0, 5, len(path)).tolist(), path)

            print("Feasible Path is found")
            dp = space.discretizePath(path)#, epsilon=5e-3)
            ts = np.linspace(0, ProjectConstants.EXECUTION_TIME, len(dp)).tolist()
            traj = Trajectory(times=ts, milestones=dp)
            vis.add("trajectory", traj)
            # self.currently_planning = False
            return traj
        else:
            print("Feasible path is not found")
            # self.currently_planning = False
            return None

    def get_ee_config(self, space, end_pos):
        robot = self.robot
        max_iter = 1000

        final_point = np.array(end_pos[:2])
        norm_fp = np.linalg.norm(final_point)
        final_point2 = np.array(final_point[:2]) * (norm_fp  - .1) / np.linalg.norm(norm_fp)
        final_point2 = final_point2.tolist() + [end_pos[2]]
        final_point = final_point.tolist() + [end_pos[2]]

        world_position = [final_point2, final_point]
        local_position = [[0,0.1,0], [0,.2,0]]


        # obj = ik.objective(robot.link(ProjectConstants.END_EFFECTOR_IND), local=[0, 0, 0], world=end_pos)
        obj = ik.objective(robot.link(ProjectConstants.END_EFFECTOR_IND), local=local_position, world=world_position)

        def feas_check():
            config = self.robot.getConfig()
            return self.validate_config(space, config)

        res = ik.solve_global(obj, iters=max_iter, startRandom=True, feasibilityCheck=feas_check)

        if res:
            # print "solution found"
            end_config = robot.getConfig()
            return end_config
        else:
            return None

    def execute_trajectory(self, input_trajectory, speed=ProjectConstants.CONTROLLER_DT * 2):
        self.executing_trajectory = True
        st = time.time()
        ct = time.time()
        while ct - st < ProjectConstants.EXECUTION_TIME - 2*speed :
            config = input_trajectory.eval(ct-st)
            self.robot.setConfig(config)
            time.sleep(speed)
            ct = time.time()

        # self.current_config = self.robot.getConfig()
        # self.robot.setConfig(self.current_config)
        self.executing_trajectory = False

    @staticmethod
    def generate_space(world, robot):
        # constraints = self.get_ik_constraints(motion)
        collider = WorldCollider(world)
        obj = ik.fixed_objective(robot.link(0))
        space = robotcspace.ClosedLoopRobotCSpace(robot, [obj], collider)
        # space = robotcspace.RobotCSpace(robot, collider=collider)
        space.setup()
        return space

    @staticmethod
    def motion_planner(qstart, qgoal, gen_space):
        settings = {'type': 'sbl',
                    'perturbationRadius': 0.25,
                    'bidirectional': True,
                    'shortcut': True,
                    'restart': False,
                    'restartTermCond': "{foundSolution:2,maxIters:4000}"
                    }
        t0 = time.time()
        print("Creating planner...")
        # Manual construction of planner
        planner = cspace.MotionPlan(gen_space, **settings)
        planner.setEndpoints(qstart, qgoal)
        print("Planner creation time", time.time() - t0)
        t0 = time.time()
        print("Planning...")

        numIters = 0
        for round in range(10):
            planner.planMore(5000)
            numIters += 1
            if planner.getPath() is not None:
                break
        print("Planning time,", numIters, "iterations", time.time() - t0)

        path = planner.getPath()
        if path is not None:
            # #######################
            path = gen_space.discretizePath(path)
            ############################
            print("Got a path with", len(path), "milestones")
        else:
            print("No feasible path was found")

        # provide some debugging information
        V, E = planner.getRoadmap()
        print(len(V), "feasible milestones sampled,", len(E), "edges connected")

        # print "CSpace stats:"
        # spacestats = space.getStats()
        # for k in sorted(spacestats.keys()):
        #     print " ",k,":",spacestats[k]
        #
        # print "Planner stats:"
        # planstats = planner.getStats()
        # for k in sorted(planstats.keys()):
        #     print " ",k,":",planstats[k]

        # if path:
        #     # visualize path as a Trajectory resource
        #     ####################################
        #     traj = RobotTrajectory(robot,np.linspace(0,10,len(path)).tolist(),path)
        # # play nice with garbage collection
        # planner.space.close()
        # planner.close()
        # return traj
        return path

    def get_hand_dictionary(self):
        robot = self.robot
        dict = {}
        for i in range(6, 40):
            dict[robot.driver(i).getName()] = i
        return dict

    def get_hand_trajectory(self, dict):#, space):
        robot = self.robot
        qold = robot.getConfig()
        milestone = 5
        # set goal of hand configuration
        robot.driver(dict.get("gripper:swivel_1")).setValue(0.5)
        robot.driver(dict.get("gripper:proximal_1")).setValue(1.4)
        robot.driver(dict.get("gripper:proximal_2")).setValue(1.4)
        robot.driver(dict.get("gripper:proximal_3")).setValue(1.4)
        qnew = robot.getConfig()
        qmin, qmax = robot.getJointLimits()
        # for i in range(len(qold)):
        #     print i,qold[i],qnew[i],qmin[i],qmax[i]
        # raw_input()
        sub = []
        for i in range(len(qnew)):
            sub.append(qnew[i] - qold[i])
        robot.setConfig(qold)
        path = []
        for i in range(milestone):
            point = []
            for j in range(len(qnew)):
                point.append(qold[j] + sub[j] / (milestone - i))
            path.append(point)
        for i in range(6 * milestone):
            path.append(path[-1])
        return path

    def solve_ik(self, robotlink, localpos, worldpos):
        """IMPLEMENT ME: solve inverse kinematics to place the 3D point
        localpos on robotlink (a RobotLink instance) at the 3D position
        worldpos in the world coordinate frame.

        Returns the robot configuration that solves the IK problem.
        """
        linkindex = robotlink.index
        robot = robotlink.robot()
        obj = ik.objective(robotlink, local=localpos, world=worldpos)
        maxIters = 100
        tol = 1e-3
        s = ik.solver(obj, maxIters, tol)
        # Set up some parameters for the numerical solver
        # Optionally you can set an initial configuration like so:
        # robot.setConfig([0]*robot.numLinks())
        # or set a random initial configuration like so:
        # s.sampleInitial()
        res = s.solve()
        # if not res:
        #     print("Couldn't solve IK problem")
        #     s.sampleInitial()
        if res:
            return robot.getConfig()
        else:
            return None

    def place_cup(self, t, R=None):
        if R is None:
            R = [1, 0, 0, 0, 1, 0, 0, 0, 1]
        self.solo_cup.setTransform(R, t)

    def visualize_world(self, world):
        vis.add("world", world)
        vis.add("coordinates", coordinates.manager())
        vis.setWindowTitle("Height Map GeneratorSuperclass World")
        vp = vis.getViewport()
        vp.w, vp.h = 600, 600
        vis.setViewport(vp)
        vis.autoFitCamera()
        vis.show()

    def get_world(self, world_file, robot_file, visualize_robot=True):
        world = WorldModel()
        res = world.readFile(world_file)
        if not res:
            raise RuntimeError("Unable to load terrain model")
        if visualize_robot:
            res = world.readFile(robot_file)
            if not res:
                raise RuntimeError("Unable to load robot model")
            current_config = world.robot(0).getConfig()
            current_config[:len(ProjectConstants.STARTING_CONFIG)] = ProjectConstants.STARTING_CONFIG
            world.robot(0).setConfig(current_config)
        return world

    @staticmethod
    def ur5_to_klampt(config):
        klampt_config = config[:]
        klampt_config.insert(0, 0.0)
        return klampt_config

    @staticmethod
    def klampt_to_ur5(config):
        return config[1:]

    @staticmethod
    def validate_config(space, config, silent=True):
        # TODO: Are the validation the same as in the robosimia
        # TODO: Is self collision detected
        # TODO: Is world object collision detected
        # TODO: Is feasibility detected
        # TODO: Is the config in the closed loop of the provided space
        try:
            for i in range(len(config)):
                if not silent:
                    print("Config #" + str(i))
                    print("space.inbounds(end_config):", space.inBounds(config[i]))
                    try:
                        print("space.closed_loop(end_config):", space.closedLoop(config[i]))
                    except AttributeError:
                        pass
                    print("space.isFeasible(end_config)", space.isFeasible(config[i]))
                    print("space.selfCollision(end_config):", space.selfCollision(config[i]))

                if space.inBounds(config[i]) and space.isFeasible(config[i]) and not space.selfCollision(
                        config[i]) and space.closedLoop(config[i]):
                    pass
                else:
                    return False
            return True

        except TypeError:
            if not silent:
                print("space.inbounds(end_config):", space.inBounds(config))
                try:
                    print("space.closed_loop(end_config):", space.closedLoop(config))
                except AttributeError:
                    pass
                print("space.isFeasible(end_config)", space.isFeasible(config))
                print("space.selfCollision(end_config):", space.selfCollision(config))
            if config is None:
                return False
            if space.inBounds(config) and space.isFeasible(config) and not space.selfCollision(
                    config):  # and space.closedLoop(config):
                return True
            else:
                return False


