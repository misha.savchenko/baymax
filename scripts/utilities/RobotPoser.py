import time
import dill
import pickle
import numpy as np
import random

from klampt import vis
from klampt import math
from klampt.math import so3
from klampt.model import ik
from klampt.plan import cspace
from klampt.model import coordinates
from klampt.plan import robotcspace
from klampt.model import trajectory
from klampt.robotsim import IKSolver
from klampt.model.collide import WorldCollider

from utilities.ProjectConstants import ProjectConstants


class RobotPoser:

    def __init__(self, DataCore):
        self.world = DataCore.get_world()
        self.robot = DataCore.get_robot()

    # CONFIGURATION METHODS

    def get_ee_config(self, space, end_pos):
        robot = self.robot
        max_iter = 1000

        # TODO: double check if the local to global transformation is the smae
        # end_pos_2 = end_pos[:]
        # end_pos_2[2] += -.1375
        # world_end_pos = [end_pos, end_pos_2]
        obj = ik.objective(robot.link(ProjectConstants.END_EFFECTOR_IND), local=[0, 0, 0], world=end_pos)

        def feas_check():
            config = self.robot.getConfig()
            return self.validate_config(space, config)

        res = ik.solve_global(obj, iters=max_iter, startRandom=True, feasibilityCheck=feas_check)

        if res:
            end_config = robot.getConfig()
            return end_config
        else:
            return None

    def get_leg_config_nearby(self, space, motion, end_pos, bias_config):
        robot = self.robot
        robot.setConfig(bias_config)
        max_iter = 1000

        f_r_active_dofs = [7, 8, 9, 10, 11, 12, 13]
        b_r_active_dofs = [15, 16, 17, 18, 19, 20, 21]
        b_l_active_dofs = [23, 24, 25, 26, 27, 28, 29]
        f_l_active_dofs = [31, 32, 33, 34, 35, 36, 37]

        leg_dictionary = {"fl": f_l_active_dofs, "fr": f_r_active_dofs, "br": b_r_active_dofs, "bl": b_l_active_dofs}

        leg_ind = leg_dictionary[motion]
        end_pos_2 = end_pos[:]
        end_pos_2[2] += -.1375
        world_end_pos = [end_pos, end_pos_2]
        obj = ik.objective(robot.link(leg_ind[-1]), local=[[0, 0, 0], [+.1375, 0, 0]], world=world_end_pos)

        def feas_check():
            config = self.robot.getConfig()
            return self.validate_config(space, config)

        # TODO: check how well the feasibility check actually work
        res = ik.solve_nearby(obj, ProjectConstants.MAX_DEVIATION_IN_NEARBY_LEG_SEARCH,
                              iters=max_iter, activeDofs=leg_ind, feasibilityCheck=feas_check)

        if res:
            end_config = robot.getConfig()
            return end_config
        else:
            return None

    def get_ee_configB(self, end_pos):
        robot = self.robot
        ik_solver = IKSolver(robot)
        ik_solver.sampleInitial()
        # ik_solver.setTolerance(0.00001)

        ik_solver.setMaxIters(1000)

        # end_pos_2 = end_pos[:]
        # end_pos_2[2] += -.1375
        # world_end_pos = [end_pos, end_pos_2]
        # self.add_line_to_vis("asdas", world_end_pos)
        # a = [[0, 0, 0], [+.1375, 0, 0]]
        # ba = robot.link(leg_ind[-1]).getWorldPosition(a[0])
        # bb = robot.link(leg_ind[-1]).getWorldPosition(a[1])
        # self.add_line_to_vis("asdasdf", [ba, bb])
        # time.sleep(10000)
        # obj = ik.objective(robot.link(leg_ind[-1]), local=[[0, 0, 0], [+.1375, 0, 0]], world=world_end_pos)

        obj = ik.objective(robot.link(ProjectConstants.END_EFFECTOR_IND), local=[0, 0, 0], world=end_pos)
        ik_solver.add(obj)

        res = ik_solver.solve()
        if res:
            end_config = robot.getConfig()
            return end_config
        else:
            return None

    def get_body_config(self, end_pose, bias_config, no_bias=False):
        end_xyz = end_pose[:3]
        end_rot = end_pose[3:]
        rollpitchyaw = [end_rot[2], end_rot[1], end_rot[0]]

        robot = self.robot
        ik_solver = IKSolver(robot)
        ik_solver.sampleInitial()
        ik_solver.setTolerance(0.00001)
        if not no_bias:
            ik_solver.setBiasConfig(bias_config)

        ik_solver.setMaxIters(1000)

        fr_active_dofs = [7, 8, 9, 10, 11, 12, 13]
        br_active_dofs = [15, 16, 17, 18, 19, 20, 21]
        bl_active_dofs = [23, 24, 25, 26, 27, 28, 29]
        fl_active_dofs = [31, 32, 33, 34, 35, 36, 37]

        leg_dictionary = {"fl": fl_active_dofs, "fr": fr_active_dofs, "br": br_active_dofs, "bl": bl_active_dofs}
        # Fixing rotation and position of the end effectors of each leg
        for key in leg_dictionary.keys():
            leg_ind = leg_dictionary[key]
            obj = ik.fixed_objective(robot.link(leg_ind[-1]))
            ik_solver.add(obj)

        end_xyz_1 = end_xyz[:]

        end_xyz_2 = end_xyz_1[:]
        end_xyz_2[2] += -.1375

        R = so3.from_rpy(rollpitchyaw)
        end_xyz_3 = so3.apply(R, [.1375, 0, 0])
        end_xyz_3 = math.vectorops.add(end_xyz_3, end_xyz_1)

        local_points = [[0, 0, 0], [0, 0, -.1375], [.1375, 0, 0]]
        world_points = [end_xyz_1, end_xyz_2, end_xyz_3]

        obj = ik.objective(robot.link(5), local=local_points, world=world_points)
        ik_solver.add(obj)

        res = ik_solver.solve()
        if res:
            return robot.getConfig()
        else:
            # print "Body config is not found"
            return None

    @staticmethod
    def validate_config(space, config, silent=True):
        # TODO: Are the validation the same as in the robosimia
        # TODO: Is self collision detected
        # TODO: Is world object collision detected
        # TODO: Is feasibility detected
        # TODO: Is the config in the closed loop of the provided space
        try:
            for i in range(len(config)):
                if not silent:
                    print("Config #" + str(i))
                    print("space.inbounds(end_config):", space.inBounds(config[i]))
                    try:
                        print("space.closed_loop(end_config):", space.closedLoop(config[i]))
                    except AttributeError:
                        pass
                    print("space.isFeasible(end_config)", space.isFeasible(config[i]))
                    print("space.selfCollision(end_config):", space.selfCollision(config[i]))

                if space.inBounds(config[i]) and space.isFeasible(config[i]) and not space.selfCollision(
                        config[i]) and space.closedLoop(config[i]):
                    pass
                else:
                    return False
            return True

        except TypeError:
            if not silent:
                print("space.inbounds(end_config):", space.inBounds(config))
                try:
                    print("space.closed_loop(end_config):", space.closedLoop(config))
                except AttributeError:
                    pass
                print("space.isFeasible(end_config)", space.isFeasible(config))
                print("space.selfCollision(end_config):", space.selfCollision(config))
            if config is None:
                return False
            if space.inBounds(config) and space.isFeasible(config) and not space.selfCollision(
                    config):# and space.closedLoop(config):
                return True
            else:
                return False

    def transform_config_to_current_frame(self, action, xy_yaw, q2):
        # # TODO doesnt really apply here but the use of coordinates transform still stands
        # robot = self.robot
        # temp_config = robot.getConfig()
        # init_motion_of_action = DataCore.Actions[action]["movement_order"][0]
        # q1 = DataCore.RobotPoser.get_action_configs(action, init_motion_of_action, initial=True)
        #
        # q1_prime = q1[:]
        # q1_prime[:2] = xy_yaw[:2]
        # q1_prime[3] = np.deg2rad(xy_yaw[2])
        #
        # robot.setConfig(q1_prime)
        # current_robot_frame = coordinates.Frame("robotOrigin", robot.link(5).getTransform())
        #
        # robot.setConfig(q1)
        # original_robot_frame = coordinates.Frame("robotOrigin", robot.link(5).getTransform())
        # transform = coordinates.Transform(current_robot_frame, original_robot_frame)
        # tf = transform.coordinates()
        #
        # new_config = q2[:]
        # new_config[0:3] = math.se3.apply(tf, new_config[0:3])
        # temp = list(math.so3.rpy(tf[0]))
        # temp[0] = math.so3.rpy(tf[0])[2]
        # temp[2] = math.so3.rpy(tf[0])[0]
        # new_config[3:6] = np.array(new_config[3:6]) + np.array(temp)
        # robot.setConfig(temp_config)
        return None

    # TRAJECTORY METHODS

    def validate_trajectory_in_bools(self, deformed_trajectory, space, silent=True):
        configs = deformed_trajectory.milestones[:]
        valid_bools = []
        for config in configs:
            valid_bools.append(self.validate_config(space, config, silent=silent))
        return valid_bools

    def execute_trajectory(self, input_trajectory, speed=ProjectConstants.CONTROLLER_DT * 2):
        for milestone in input_trajectory.milestones:
            self.robot.setConfig(milestone)
            time.sleep(speed)

    def generate_body_primitive(self, start_config, end_config):
        configs = [start_config, end_config]
        self.robot.setConfig(start_config)
        space = self.generate_space("bd")

        if not self.validate_config(space, configs):
            print("generate_body_primitive: Configurations are Invalid")
            return None

        constraints = self.get_ik_constraints("bd")
        space.close()
        discretized_path = self.closed_loop_planner(configs, constraints)
        if discretized_path is None:
            return None
        return discretized_path

    def generate_leg_primitive(self, init_config, final_config, leg, planner="sbl"):
        # world = self.world
        # robot = self.robot

        # init = self.feet_xyz(init_config, True)[leg]
        # final = self.feet_xyz(final_config, True)[leg]
        # final = math.vectorops.add(init, delta)

        # points = self.step_function(init, final, step_height)
        milestones = []
        # for p in points[1:-1]:
        #     milestones.append(self.get_leg_config(leg, p))
        self.robot.setConfig(init_config)
        configs = [init_config, final_config]
        space = self.generate_space(leg)
        constraints = self.get_ik_constraints(leg)

        if not self.validate_config(space, configs + milestones, silent=True):
            print("Invalid configs found in the generate_leg_primitive")
            # if not self.validate_config(space, milestones, silent=True):
            #     print "Invalid milestones"
            if not self.validate_config(space, milestones, silent=True):
                print("Invalid start and end configs")
                # self.validate_config(space, configs, silent=False)
                return None

        space.close()
        discretized_path = self.closed_loop_planner(configs, constraints, algorithm=planner, milestones=None)
        if discretized_path is None:
            return None
        return discretized_path

    def reset_robot(self):
        self.robot.setConfig(ProjectConstants.STARTING_CONFIG)

    # TODO: Add clearance for leg foottrajectories

    # OTHER METHODS

    def get_ik_constraints(self):
        return ik.fixed_objective(self.robot.link(0))

    def generate_space(self):
        robot = self.robot
        world = self.world
        # TODO: Constraints should only not exclude any joints
        # constraints = self.get_ik_constraints(motion)
        collider = WorldCollider(world)
        # space = robotcspace.ClosedLoopRobotCSpace(robot, None, collider)
        space = robotcspace.RobotCSpace(robot, collider=collider)
        space.setup()
        return space

    def closed_loop_planner(self, configs, constraints, algorithm="sbl", milestones=None):
        # Planners:
        #           prm, lazyprm, prm*, lazyprm*, spars
        #           rrt, rrtconnect, birrt, lazyrrt, lbtrrt, rrt*, informedrrt*
        #           est, fmt, sbl, stride

        world = self.world
        robot = self.robot

        settings = {'type': algorithm, 'perturbationRadius': 0.1, 'bidirectional': 1, 'shortcut': 0, 'restart': 0,
                    'restartTermCond': "{foundSolution:1,maxIters:4000}"}

        m_planner_ik_constraints = constraints
        collider = WorldCollider(world)
        space = robotcspace.ClosedLoopRobotCSpace(robot, m_planner_ik_constraints, collider)
        space.setup()

        if not self.validate_config(space, configs, silent=True):
            print("closed_loop_planner: Configurations are Invalid")
            return None

        plan = cspace.MotionPlan(space)
        plan.setEndpoints(configs[0], configs[-1])
        if milestones is not None:
            for m in milestones:
                plan.addMilestone(m)

        plan.setOptions(**settings)
        plan.planMore(4000)

        V, E = plan.getRoadmap()

        print(plan.getStats())
        print(len(V), "feasible milestones sampled,", len(E), "edges connected")

        path = plan.getPath()
        dp = space.discretizePath(path, epsilon=5e-3)
        plan.space.close()
        plan.close()
        space.close()
        return dp

    def open_loop_planner(self, configs, constraints=None, algorithm="lazyprm*", leg=None):
        world = self.world
        robot = self.robot

        collider = WorldCollider(world)
        space = robotcspace.RobotCSpace(robot, collider=collider)

        # def in_triangle(q):
        #     if leg is not None:
        #         return self.is_in_triangle(robot, leg)
        #     else:
        #         return True
        # space.addConstraint(in_triangle)

        if constraints is not None:
            space.addConstraint(constraints)

        # space.setup()

        algo = algorithm
        settings = {'type': algo, 'perturbationRadius': 0.5, 'bidirectional': 1, 'shortcut': 0, 'restart': 2    ,
                    'restartTermCond': "{foundSolution:1,maxIters:2000}"}
        start_config = configs[0]
        robot.setConfig(start_config)
        end_config = configs[-1]

        # adding mile stones doesnt work
        cspace.MotionPlan.setOptions(**settings)
        plan = cspace.MotionPlan(space)
        plan.setEndpoints(start_config, end_config)

        if len(configs) > 2:
            for i in range(1, len(configs) - 1):
                plan.addMilestone(configs[i])

        # plan.planMore(2000)

        V, E = plan.getRoadmap()
        path = plan.getPath()
        plan.close()
        print(path)
        discritized_path = []

        for i in range(0, 101):
            u = float(i) / 100
            discritized_path.append(space.interpolate(start_config, end_config, u))
        space.close()

        return discritized_path

    # LOAD METHODS

    @staticmethod
    def get_pickled_obj(save_location):
        s_time = time.time()
        obj = None
        try:
            obj = pickle.load(open((save_location + ".pickle"), "rb"))
        except EOFError:
            try:
                obj = dill.load(open((save_location + ".dill"), "rb"))
            except EOFError:
                print("Error loading:" + save_location + " FAILED")
        print("Retrieved pickled file:", save_location, "in:", time.time() - s_time, "s")
        return obj

    @staticmethod
    def get_trajectory(primitive_file):
        try:
            primitive_trajectory = trajectory.Trajectory()
            primitive_trajectory.load(primitive_file)
            return primitive_trajectory
        except IOError:
            print("PRIMITIVE DOES NOT EXIST")
            return None

    # VIS METHODS

    def cycle_through_configs(self, configs, config_names):
        while True:
            for c in range(len(configs)):
                vis.addText("config_cycle", config_names[c])
                self.robot.setConfig(configs[c])
                time.sleep(1)

    def vis_support_triangle(self, swing_leg, feet_dictionary=None, name=None):
        robot = self.robot
        if feet_dictionary is None:
            [fr, br, bl, fl] = self.feet_xyz(robot.getConfig())

        else:
            br = feet_dictionary["br"]
            fr = feet_dictionary["fr"]
            fl = feet_dictionary["fl"]
            bl = feet_dictionary["bl"]

        leg_dictionary = {"br": [fr, bl, fl, fr],
                          "fr": [br, bl, fl, br],
                          "bl": [fr, br, fl, fr],
                          "fl": [fr, bl, br, fr]}

        traj = trajectory.Trajectory(milestones=leg_dictionary[swing_leg])
        if name is None:
            vis.add("Support Triangle", traj)
        else:
            vis.add(name, traj)

    @staticmethod
    def add_line_to_vis(name, points):
        if len(points) >= 2:
            traj = trajectory.Trajectory(milestones=points)
        else:
            points = [points]
            point2 = points[:]
            point2[2] += 1
            traj = trajectory.Trajectory(milestones=[points, point2])
        vis.add(name, traj)

    def vis_points(self, points, name_mod=None, color="Yellow"):
        for point in points:
            a = point
            b = a[:]
            b[2] = -2
            if name_mod is None:
                name = str(random.randint(0, 10000))
            else:
                name = name_mod + "_" + str(random.randint(0, 10000))
            self.add_line_to_vis(name, [b, a])
            if color == "green":
                vis.setColor(name, 0, 1, 0)
            elif color == "red":
                vis.setColor(name, 1, 0, 0)

    def save_trajectory(self, number_of_saved, traj, action, motion, index):
        name = str(number_of_saved) + "_" + str(action) + "_" + str(motion) + "_" + str(index)
        try:
            # fileObject = open("data/primitives/" + name + "_traj", 'w')
            traj.save("data/paths/" + name + ".traj")
        except IOError:
            print("PRIMITIVE DOES NOT EXIST")
            return None
