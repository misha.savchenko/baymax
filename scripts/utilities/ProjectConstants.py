import os
import math


class ProjectConstants(object):
    # STARTING_CONFIG
    CONTROLLER_DT = .005

    # Starting config for ur5_with_gripper.rob file
    # STARTING_CONFIG = [0.0, 0.0, -1.5707963267948966, 0.0, 0.0, 1.5707963267948966,
    #                    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    # Starting config for ur5.rob file
    STARTING_CONFIG = [0.0, 0.0, -1.5707963267948966, 0.0, -1.5707963267948966, 0.0, 0.0, 0.0]
    # HOME_CONFIG = [0.0, 0.0, -1.5707963267948966, 0.0, 0.0, 0.0, 0.0, 0.0]

    END_EFFECTOR_IND = 6

    EXECUTION_TIME = 10

    # DIRECTORY LIST
    dir_path = os.path.dirname(os.path.realpath(__file__))
    
    world_file = dir_path + "/../data/world/" + "flatworld" + ".xml"
    robot_file = dir_path + "/../../data/robots/ur5.rob"
    solo_cup_obj = dir_path + "/../../data/worlds/objects/better_cup.off"


    robot_ip_adress = "192.168.0.100"
