import time
import numpy as np


class Controller:

    def __init__(self, robot, t0, P=0.2, I=0.0, D=0.0):
        self.robot = robot
        self.t0 = t0
        self.t_last = t0

        self.err_last = 0

        self.Kp = P
        self.Ki = I
        self.Kd = D

        self.p_control = 0
        self.i_control = 0
        self.d_control = 0

    def pid_controller(self, qd, q):
        if not isinstance(qd, np.ndarray):
            qd = np.array(qd)

        if not isinstance(q, np.ndarray):
            q = np.array(q)

        t = time.time()
        dt = t - self.t_last
        err = qd - q
        derr = err - self.err_last

        self.p_control = self.Kp * err
        self.i_control += err * dt
        self.d_control = derr / dt

        self.t_last = t
        self.err_last = err
        u = self.p_control + self.Ki * self.i_control - (self.Kd * self.d_control)
        return u
