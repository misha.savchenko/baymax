#!/usr/bin/env python

# import rospy
# from baymax.msg import object_pose

import numpy as np
import argparse
import imutils
from imutils.video import FPS
import time
import cv2
import os
import time
import pyrealsense2 as rs
import json
import math

from multiprocessing.connection import Client
c = Client(('localhost', 5000))


def getCentroid():
    # The following function acquires centroid coordinates, and bounding box dimensions for each frame
    def dataAcq(BL_dist, x, y, BL_point, TL_dist, w, h, TL_point, widthData, htData, dataPoint, depth_point, data,
                counter):
        BL_dist = depth_frame.get_distance(x, y)
        BL_dist = BL_dist + 0.0406527
        BL_point = rs.rs2_deproject_pixel_to_point(depth_intrin, [x, y], BL_dist)

        TL_dist = depth_frame.get_distance(x + w, y + h)
        TL_dist = TL_dist + 0.0406527
        TL_point = rs.rs2_deproject_pixel_to_point(depth_intrin, [x + w, y + h], TL_dist)

        widthData = abs(TL_point[0] - BL_point[0])
        htData = abs(TL_point[1] - BL_point[1])
        dataPoint = {'w': widthData, 'h': htData, 'x': depth_point[0], 'y': depth_point[1], 'd': depth_point[2]}
        data[counter] = dataPoint
        counter += 1

    def drawRect(x, y, w, h, color, text, centerX, centerY, depth_point, font, fontScale, fontColor, lineType):
        cv2.rectangle(color_image, (x, y), (x + w, y + h), color, 2)
        cv2.circle(color_image, (centerX, centerY), 3, (255, 255, 255), -1)
        cv2.circle(depth_image, (centerX, centerY), 3, (255, 255, 255), -1)
        cv2.rectangle(depth_image, (x, y), (x + w, y + h), color, 2)
        cv2.putText(color_image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
        cv2.putText(depth_image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
        V = np.zeros((3, 1), dtype=np.float)
        # The following 3 lines "zero" each axis (center of mounting plate on surface of table)
        V[0][0] = depth_point[0] - 0.01905
        V[1][0] = (depth_point[1] + 0.24765) * -1
        V[2][0] = depth_point[2] - 0.0042 - 0.5715
        return V

    # The following function rotates the camera's X, Y, and Z axes to align with the X, Y, and Z axes used for motion planning
    def calibrateAxes(unitVec):
        R1 = np.zeros((3, 3), dtype=np.float)
        R2 = np.zeros((3, 3), dtype=np.float)
        R3 = np.zeros((3, 3), dtype=np.float)
        theta1 = math.radians(180)
        theta2 = math.radians(90)

        # Rotate 180-degrees about camera's (downward pointing) Y-axis:
        R1[0][0] = math.cos(theta1)
        R1[0][1] = 0
        R1[0][2] = math.sin(theta1)
        R1[1][0] = 0
        R1[1][1] = 1
        R1[1][2] = 0
        R1[2][0] = -math.sin(theta1)
        R1[2][1] = 0
        R1[2][2] = math.cos(theta1)
        # Now, X points left, Y points down, Z points in (towards camera)

        # Rotate 90-degrees about camera's (left-pointing) X-axis
        R2[0][0] = 1
        R2[0][1] = 0
        R2[0][2] = 0
        R2[1][0] = 0
        R2[1][1] = math.cos(theta2)
        R2[1][2] = -math.sin(theta2)
        R2[2][0] = 0
        R2[2][1] = math.sin(theta2)
        R2[2][2] = math.cos(theta2)
        R3 = np.matmul(R1, R2)
        unitVec = np.matmul(R3, unitVec)
        # Now, X points left, Y points in (towards camera), Z points up
        return unitVec

    # The following function rotates the motion planner's axes by user-defined angles
    def rotate(thetaX, thetaY, thetaZ):
        RX = np.zeros((3, 3), dtype=np.float)
        RY = np.zeros((3, 3), dtype=np.float)
        RZ = np.zeros((3, 3), dtype=np.float)
        R1 = np.zeros((3, 3), dtype=np.float)
        R2 = np.zeros((3, 3), dtype=np.float)
        thetaXDeg = math.radians(thetaX)
        thetaYDeg = math.radians(thetaY)
        thetaZDeg = math.radians(thetaZ)

        # Rotation about X-axis
        RX[0][0] = 1
        RX[0][1] = 0
        RX[0][2] = 0
        RX[1][0] = 0
        RX[1][1] = math.cos(thetaXDeg)
        RX[1][2] = -math.sin(thetaXDeg)
        RX[2][0] = 0
        RX[2][1] = math.sin(thetaXDeg)
        RX[2][2] = math.cos(thetaXDeg)

        # Rotation about Y-axis
        RY[0][0] = math.cos(thetaYDeg)
        RY[0][1] = 0
        RY[0][2] = math.sin(thetaYDeg)
        RY[1][0] = 0
        RY[1][1] = 1
        RY[1][2] = 0
        RY[2][0] = -math.sin(thetaYDeg)
        RY[2][1] = 0
        RY[2][2] = math.cos(thetaYDeg)

        # Rotation about Z-axis
        RZ[0][0] = math.cos(thetaZDeg)
        RZ[0][1] = -math.sin(thetaZDeg)
        RZ[0][2] = 0
        RZ[1][0] = math.sin(thetaZDeg)
        RZ[1][1] = math.cos(thetaZDeg)
        RZ[1][2] = 0
        RZ[2][0] = 0
        RZ[2][1] = 0
        RZ[2][2] = 1

        R1 = np.matmul(RZ, RY)
        R2 = np.matmul(R1, RX)
        return R2

    # The following function returns coordinates in the motion planner's frame of reference
    def rotatedResult(R, V):
        return np.matmul(R, V)

    # Construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    # ap.add_argument("-o1", "--output1", required=True,
    #                 help="path to output video1")
    # ap.add_argument("-o2", "--output2", required=True,
    #                 help="path to output video2")
    # ap.add_argument("-y", "--yolo", required=True,
    #                 help="base path to YOLO directory")
    ap.add_argument("-c", "--confidence", type=float, default=0.5,
                    help="minimum probability to filter weak detections")
    ap.add_argument("-t", "--threshold", type=float, default=0.3,
                    help="threshold when applyong non-maxima suppression")
    args = vars(ap.parse_args())

    # Load the COCO class labels our YOLO model was trained on
    # labelsPath = os.path.sep.join([args["yolo"], "coco.names"])
    yolo_path = "src/baymax/scripts/data/yolo-coco/"
    labelsPath = yolo_path + "coco.names"
    LABELS = open(labelsPath).read().strip().split("\n")

    # Initialize a list of colors to represent each possible class label
    np.random.seed(42)
    COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
                               dtype="uint8")

    # Derive the paths to the YOLO weights and model configuration
    # weightsPath = os.path.sep.join([args["yolo"], "yolov3.weights"])
    weightsPath = yolo_path + "yolov3.weights"
    # configPath = os.path.sep.join([args["yolo"], "yolov3.cfg"])
    configPath = yolo_path + "yolov3.cfg"

    # Load our YOLO object detector trained on COCO dataset (80 classes) and determine only the *output* layer names that we need from 		YOLO
    # print("[INFO] loading YOLO from disk...")
    net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
    ln = net.getLayerNames()
    ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    writer1 = None
    writer2 = None
    (W, H) = (None, None)

    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 60)
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 60)

    # Start streaming
    profile = pipeline.start(config)

    data = {}
    counter = 1

    # Set parameters for displaying text
    font = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = (0, 15)
    fontScale = 0.5
    fontColor = (250, 250, 250)
    lineType = 2

    t_end = 10
    start_time = time.time()

    # while time.time() - start_time < t_end:
    while True:
        # while True:
        # print("****")
        startTime = time.time()
        # print("Start:", startTime)

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()

        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())

        (H, W) = color_image.shape[:2]
        blob = cv2.dnn.blobFromImage(color_image, 1 / 255.0, (416, 416), swapRB=True, crop=False)
        net.setInput(blob)
        # print("Before:", time.time())
        layerOutputs = net.forward(ln)
        # print("After:", time.time())
        boxes = []
        confidences = []
        classIDs = []

        for output in layerOutputs:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]
                if confidence > args["confidence"]:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        idxs = cv2.dnn.NMSBoxes(boxes, confidences, args["confidence"], args["threshold"])
        V_tformed  = np.array([[5],[5],[5]])
        if len(idxs) > 0:
            for i in idxs.flatten():
                text = "{}: {:.4f}".format(LABELS[classIDs[i]], confidences[i])
                if "cup" not in text:
                    continue

                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                color = [int(c) for c in COLORS[classIDs[i]]]
                dist = depth_frame.get_distance(centerX, centerY)
                dist = dist + 0.0406527  # Adds distance between front of cup and its centroid inside the cup

                depth_intrin = depth_frame.profile.as_video_stream_profile().intrinsics
                depth_point = rs.rs2_deproject_pixel_to_point(depth_intrin, [centerX, centerY], dist)

                # Data acquisition
                BL_dist, BL_point, TL_dist, TL_point, widthData, htData = [0, 0, 0, 0, 0, 0]
                dataPoint = {'w': widthData, 'h': htData, 'x': depth_point[0], 'y': depth_point[1], 'd': depth_point[2]}
                dataAcq(BL_dist, x, y, BL_point, TL_dist, w, h, TL_point, widthData, htData, dataPoint, depth_point,
                        data, counter)

                # Draw bounding box
                V = drawRect(x, y, w, h, color, text, centerX, centerY, depth_point, font, fontScale, fontColor,
                             lineType)

                # Coordinate transformation
                R = rotate(0, 0, 0)  # Angles are in DEGREES
                V_tformed = rotatedResult(R, calibrateAxes(V))  # This info gets published in the following line
                # Display 3-D centroid coordinates (in METERS) in motion planner's frame of reference
                str3D = "(%.3f, %.3f, %.3f)" % (V_tformed[0][0], V_tformed[1][0], V_tformed[2][0])
                cv2.putText(color_image, str3D, bottomLeftCornerOfText, font, fontScale, fontColor, lineType)
                cv2.putText(depth_image, str3D, bottomLeftCornerOfText, font, fontScale, fontColor, lineType)

        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.3), cv2.COLORMAP_JET)
        images = np.hstack((color_image, depth_colormap))
        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', images)
        cv2.waitKey(1)
        # if writer1 is None and writer2 is None:
        #     fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        #     writer1 = cv2.VideoWriter(args["output1"], fourcc, 1.5, (color_image.shape[1], color_image.shape[0]), True)
        #     writer2 = cv2.VideoWriter(args["output2"], fourcc, 1.5, (depth_colormap.shape[1], depth_colormap.shape[0]),
        #                               True)
        # writer1.write(color_image)
        # writer2.write(depth_colormap)
        # endTime = time.time()
        return V_tformed

    # print("End:", endTime)
    # print("DURATION:", endTime-startTime)

    # print("[INFO] cleaning up...")
    # with open('data.json', 'w') as outfile:
    #     json.dump(data, outfile, sort_keys=True, indent=4, ensure_ascii=False)
    # pipeline.stop()
    # writer1.release()
    # writer2.release()
    # cv2.destroyAllWindows()

def talker2():
    while True:
        vec = getCentroid()
        pose = vec.T.tolist()[0] + [1, 1, 1]
        pose = str(pose)
        pickle.dump(your_object, your_file, protocol=2)

        c.send("hello")
        # print("Got", c.recv())
    # pub = rospy.Publisher('object_pose', object_pose, queue_size=1)
    # rospy.init_node('object_pose', anonymous=True)
    # rate = rospy.Rate(10)  # 10hz
    # custom_message = object_pose()
    # while not rospy.is_shutdown():
    #     custom_message.object = "cup"
    #     vec = getCentroid()
    #     pose = vec.T.tolist()[0] + [1, 1, 1]
    #     custom_message.pose = pose
    #
    #     rospy.loginfo(custom_message)
    #     pub.publish(custom_message)
    #     rate.sleep()

if __name__ == '__main__':
    # try:
    talker2()
    # except rospy.ROSInterruptException:
    #     pass
